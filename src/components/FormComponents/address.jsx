import React from 'react';
import valueMixin from './mixins/valueMixin';
import selectMixin from './mixins/selectMixin';
import componentMixin from './mixins/componentMixin';
import debounce from 'lodash/debounce';

module.exports = React.createClass({
  displayName: 'Address',
  mixins: [valueMixin, selectMixin, componentMixin],
  getTextField: function() {
    return 'formatted_address';
  },
  getValueField: function() {
    return null;
  },
  refreshItems: function(criteria) {
    this.doSearch(criteria);
  },
  doSearch: debounce(function(text) {
    let mapsApi = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + text;
    if (this.props.component.map.region) {
      mapsApi = mapsApi + '&region=' + this.props.component.map.region;
    }
    mapsApi = mapsApi + '&sensor=false';
    fetch(mapsApi)
      .then(function(response) {
        response.json().then(function(data) {
          this.setState({
            selectItems: data.results
          });
        }.bind(this));
      }.bind(this));
  }, 200),
  getValueDisplay: function(component, data) {
    return data ? data.formatted_address : '';
  }
});
