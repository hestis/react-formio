import React, { Component } from 'react';
import { DateField, DatePicker, Footer } from 'react-date-picker';
import MaskedInput from 'react-text-mask';
import moment from 'moment';
import 'moment/locale/pt-br';

import valueMixin from './mixins/valueMixin';
import componentMixin from './mixins/componentMixin';

module.exports = React.createClass({
  displayName: 'Datetime',
  mixins: [valueMixin, componentMixin],

  getInitialValue: function() {
    const { component } = this.props;
    if (component.hasOwnProperty('defaultDate') && component.defaultDate) {
      return moment(component.defaultDate, component.format);
    }
    return '';
  },

  timeOnly: function() {
    const { enableDate, enableTime } = this.props.component;
    return enableTime && !enableDate;
  },

  onDateChange: function(newValue) {
    this.setValue(newValue);
  },

  validateDate: function() {
    const { format } = this.props.component;
    const { value } = this.state;
    if (value && !moment(value, format, true).isValid()) {
      this.setState({
        isValid: false,
        errorType: 'InvalidDate',
        errorMessage: `Choose a valid Date For Format ${format}`,
      });
      this.forceUpdate();
    }
  },

  getInputMask: function(mask) {
    if (typeof this.customMask === 'function') {
      return this.customMask;
    }
    if (!mask) {
      return false;
    }
    if (mask instanceof Array) {
      return mask;
    }
    let maskArray = [];
    for (let i=0; i < mask.length; i++) {
      switch (mask[i]) {
        case '9':
          maskArray.push(/\d/);
          break;
        case 'a':
        case 'A':
          maskArray.push(/[a-zA-Z]/);
          break;
        case '*':
          maskArray.push(/[a-zA-Z0-9]/);
          break;
        default:
          maskArray.push(mask[i]);
          break;
      }
    }
    return maskArray;
  },

  renderCalendarIcon: function({ onMouseDown }) {
    return (
      <span className="input-group-btn" key="buttons">
        <button type="button" className="btn btn-default" onClick={onMouseDown}>
          { this.props.component.enableDate ?
            <i className="glyphicon glyphicon-calendar" /> :
            <i className="glyphicon glyphicon-time" />
          }
        </button>
      </span>
    )
  },

  renderInput: function(inputProps) {
    const propsForInput = {
      ...inputProps,
      guide: true,
      mask: this.getInputMask(this.props.component.inputMask),
      onKeyDown: () => undefined,
    };
    return (
      <MaskedInput
        type="text"
        {...propsForInput}
      />
    );
  },

  getElements: function() {
    const {
      label,
      format,
      enableTime,
      datePicker,
      placeholder,
    } = this.props.component;
    const classNames = this.timeOnly() ? 'time-picker-component' : '';
    return (
      <div className={classNames} >
        <label className="control-label">{label}</label>
        <DateField
          rawInput
          updateOnDateClick
          collapseOnDateClick
          dateFormat={format}
          validateOnBlur={false}
          className="form-control"
          value={this.state.value}
          placeholder={placeholder}
          minDate={datePicker.minDate}
          maxDate={datePicker.maxDate}
          renderInput={this.renderInput}
          onTextChange={this.onDateChange}
          renderCalendarIcon={this.renderCalendarIcon}
          onBlur={this.validateDate}
        >
          <DatePicker
            weekNumbers
            highlightToday
            weekStartDay={0}
            highlightWeekends
            clearButton={false}
            okButton={enableTime}
            showClock={enableTime}
            onChange={this.onDateChange}
            navigation={!this.timeOnly()}
            todayButton={!this.timeOnly()}
          />
        </DateField>
      </div>
    );
  },
});
