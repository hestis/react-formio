'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDatePicker = require('react-date-picker');

var _reactTextMask = require('react-text-mask');

var _reactTextMask2 = _interopRequireDefault(_reactTextMask);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

require('moment/locale/pt-br');

var _valueMixin = require('./mixins/valueMixin');

var _valueMixin2 = _interopRequireDefault(_valueMixin);

var _componentMixin = require('./mixins/componentMixin');

var _componentMixin2 = _interopRequireDefault(_componentMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = _react2.default.createClass({
  displayName: 'Datetime',
  mixins: [_valueMixin2.default, _componentMixin2.default],

  getInitialValue: function getInitialValue() {
    var component = this.props.component;

    if (component.hasOwnProperty('defaultDate') && component.defaultDate) {
      return (0, _moment2.default)(component.defaultDate, component.format);
    }
    return '';
  },

  timeOnly: function timeOnly() {
    var _props$component = this.props.component,
        enableDate = _props$component.enableDate,
        enableTime = _props$component.enableTime;

    return enableTime && !enableDate;
  },

  onDateChange: function onDateChange(newValue) {
    this.setValue(newValue);
  },

  validateDate: function validateDate() {
    var format = this.props.component.format;
    var value = this.state.value;

    if (value && !(0, _moment2.default)(value, format, true).isValid()) {
      this.setState({
        isValid: false,
        errorType: 'InvalidDate',
        errorMessage: 'Choose a valid Date For Format ' + format
      });
      this.forceUpdate();
    }
  },

  getInputMask: function getInputMask(mask) {
    if (typeof this.customMask === 'function') {
      return this.customMask;
    }
    if (!mask) {
      return false;
    }
    if (mask instanceof Array) {
      return mask;
    }
    var maskArray = [];
    for (var i = 0; i < mask.length; i++) {
      switch (mask[i]) {
        case '9':
          maskArray.push(/\d/);
          break;
        case 'a':
        case 'A':
          maskArray.push(/[a-zA-Z]/);
          break;
        case '*':
          maskArray.push(/[a-zA-Z0-9]/);
          break;
        default:
          maskArray.push(mask[i]);
          break;
      }
    }
    return maskArray;
  },

  renderCalendarIcon: function renderCalendarIcon(_ref) {
    var onMouseDown = _ref.onMouseDown;

    return _react2.default.createElement(
      'span',
      { className: 'input-group-btn', key: 'buttons' },
      _react2.default.createElement(
        'button',
        { type: 'button', className: 'btn btn-default', onClick: onMouseDown },
        this.props.component.enableDate ? _react2.default.createElement('i', { className: 'glyphicon glyphicon-calendar' }) : _react2.default.createElement('i', { className: 'glyphicon glyphicon-time' })
      )
    );
  },

  renderInput: function renderInput(inputProps) {
    var propsForInput = _extends({}, inputProps, {
      guide: true,
      mask: this.getInputMask(this.props.component.inputMask),
      onKeyDown: function onKeyDown() {
        return undefined;
      }
    });
    return _react2.default.createElement(_reactTextMask2.default, _extends({
      type: 'text'
    }, propsForInput));
  },

  getElements: function getElements() {
    var _props$component2 = this.props.component,
        label = _props$component2.label,
        format = _props$component2.format,
        enableTime = _props$component2.enableTime,
        datePicker = _props$component2.datePicker,
        placeholder = _props$component2.placeholder;

    var classNames = this.timeOnly() ? 'time-picker-component' : '';
    return _react2.default.createElement(
      'div',
      { className: classNames },
      _react2.default.createElement(
        'label',
        { className: 'control-label' },
        label
      ),
      _react2.default.createElement(
        _reactDatePicker.DateField,
        {
          rawInput: true,
          updateOnDateClick: true,
          collapseOnDateClick: true,
          dateFormat: format,
          validateOnBlur: false,
          className: 'form-control',
          value: this.state.value,
          placeholder: placeholder,
          minDate: datePicker.minDate,
          maxDate: datePicker.maxDate,
          renderInput: this.renderInput,
          onTextChange: this.onDateChange,
          renderCalendarIcon: this.renderCalendarIcon,
          onBlur: this.validateDate
        },
        _react2.default.createElement(_reactDatePicker.DatePicker, {
          weekNumbers: true,
          highlightToday: true,
          weekStartDay: 0,
          highlightWeekends: true,
          clearButton: false,
          okButton: enableTime,
          showClock: enableTime,
          onChange: this.onDateChange,
          navigation: !this.timeOnly(),
          todayButton: !this.timeOnly()
        })
      )
    );
  }
});