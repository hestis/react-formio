"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var addLabelAndDescription = function addLabelAndDescription(Component) {
  var LabelDescription = function LabelDescription(props) {
    return _react2.default.createElement(
      "div",
      { className: "form-group" },
      _react2.default.createElement(
        "label",
        { className: "control-label" },
        props.component.label
      ),
      _react2.default.createElement(Component, props),
      _react2.default.createElement(
        "div",
        { className: "help-block" },
        _react2.default.createElement(
          "span",
          null,
          props.component.description
        )
      )
    );
  };
  return LabelDescription;
};

exports.default = addLabelAndDescription;